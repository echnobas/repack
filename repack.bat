@echo off
if not exist %CD%\out mkdir %CD%\out || goto :error
cd %CD%\out || goto :error
del /F /Q /S * >nul 2>&1 || goto :error
cd .. || goto :error
set FILE_TARGET=%1
echo [1] Generating keys..
C:\Users\%USERNAME%\.cargo\bin\cargo b --release --quiet || goto :error
echo [2] Starting pack..
target\release\encrypt.exe || goto :error
echo [3] Pack success..
move /y %FILE_TARGET%.enc %CD%\out >nul 2>&1 || goto :error
copy target\release\decrypt.exe %CD%\out\decrypt.exe >nul 2>&1 || goto :error
echo [4] Packing complete, see %CD%\out
goto :EOF

:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%