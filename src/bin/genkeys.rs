use rand::rngs::OsRng;
use rand::RngCore;
use std::fs::OpenOptions;
use std::io::Write;

const KEY_SZ: usize = 32;
const NONCE_SZ: usize = 19;

fn main() {
    // let mut key = [0u8; 32];
    // let mut nonce = [0u8; 19];
    // OsRng.fill_bytes(&mut key);
    // OsRng.fill_bytes(&mut nonce);
    let mut key_and_nonce = [0u8; KEY_SZ + NONCE_SZ];
    OsRng.fill_bytes(&mut key_and_nonce);

    OpenOptions::new()
        .create(true)
        .write(true)
        .open("keys.dat")
        .and_then(move |mut f| f.write_all(&key_and_nonce))
        .expect("writing keys failed");
}
