use std::{
    fmt::Display,
    fs::{self, File},
    io::Seek,
    os::windows::prelude::MetadataExt,
    path::PathBuf,
    process,
};

use chacha20poly1305::{aead::stream, KeyInit, XChaCha20Poly1305};

const BUFFER_SZ: usize = 1 * 1000;

trait Abort<T> {
    fn abort<S: Display>(self, msg: S) -> T;
}

impl<T> Abort<T> for Option<T> {
    fn abort<S: Display>(self, msg: S) -> T {
        if self.is_none() {
            eprintln!("ERROR!: {msg}");
            process::exit(1);
        }
        unsafe { self.unwrap_unchecked() }
    }
}
impl<T, E> Abort<T> for Result<T, E> {
    fn abort<S: Display>(self, msg: S) -> T {
        if self.is_err() {
            eprintln!("ERROR!: {msg}");
            process::exit(1);
        }
        unsafe { self.unwrap_unchecked() }
    }
}

fn main() {
    let decrypted_source_path = std::env::args()
        .skip(1)
        .next()
        .map(PathBuf::from)
        .abort("Couldn't get encrypted source path");

    let encrypted_dest_path = {
        let mut p = decrypted_source_path.parent().map(PathBuf::from).unwrap();
        p.push(
            decrypted_source_path
                .file_name()
                .and_then(|s| s.to_str())
                .map(|s| format!("{}.enc", s))
                .abort("Couldn't convert string to OsStr"),
        );
        p
    };

    let (key, nonce) = load_key_nonce().abort("Failed to load keys");
    println!("Using key: {:?}\n    nonce: {:?}", key, nonce);

    let mut src = File::options()
        .read(true)
        .open(&decrypted_source_path)
        .abort("Couldn't open decrypted source file, does it exist?");

    let mut dst = File::options()
        .create(true)
        .read(true)
        .write(true)
        .open(&encrypted_dest_path)
        .abort("Could not open destination file");

    let mut progress = linya::Progress::new();
    let bar = progress.bar(
        src.metadata()
            .map(|m| m.file_size())
            .abort("Failed to read file metadata") as usize,
        format!(
            "Encrypting {} -> {}",
            decrypted_source_path.display(),
            encrypted_dest_path.display()
        ),
    );

    encrypt(&mut src, &mut dst, &key, &nonce, (&mut progress, &bar));

    println!(
        "Successfully encrypted {} -> {}",
        decrypted_source_path.display(),
        encrypted_dest_path.display()
    );
}

fn load_key_nonce() -> std::io::Result<([u8; 32], [u8; 19])> {
    fs::read("keys.dat").map(|b| {
        assert_eq!(b.len(), 51);
        let (mut key, mut nonce) = ([0u8; 32], [0u8; 19]);
        key.copy_from_slice(&b[..32]);
        nonce.copy_from_slice(&b[32..51]);
        (key, nonce)
    })
}

fn encrypt<R: std::io::Read + Seek, W: std::io::Write>(
    mut source: R,
    mut dest: W,
    key: &[u8; 32],
    nonce: &[u8; 19],
    (progress, bar): (&mut linya::Progress, &linya::Bar),
) {
    let aead = XChaCha20Poly1305::new(key.as_ref().into());
    let mut stream_encryptor = stream::EncryptorBE32::from_aead(aead, nonce.as_ref().into());

    let mut buffer = [0u8; BUFFER_SZ];
    loop {
        let read = source.read(&mut buffer).abort("Failed to read into buffer");
        
        progress.inc(&bar, read);

        if read == BUFFER_SZ {
            // Still reading
            let cipher_text = stream_encryptor
                .encrypt_next(buffer.as_slice())
                .abort("Failed to encrypt buffer");
            dest.write(&cipher_text)
                .abort("Failed to write to destination file");
            progress.draw(&bar);
        } else {
            // Finish
            let cipher_text = stream_encryptor
                .encrypt_last(&buffer[..read])
                .abort("Failed to decrypt buffer");
            dest.write(&cipher_text)
                .abort("Failed to write to destination file");
            progress.draw(&bar);
            break;
        }
    }
}
