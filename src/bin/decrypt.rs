use std::{
    fmt::Display,
    fs::{self, File},
    path::{PathBuf},
    process,
};

use chacha20poly1305::{aead::stream, KeyInit, XChaCha20Poly1305};

const BUFFER_SZ: usize = 1 * 1000;

trait Abort<T> {
    fn abort<S: Display>(self, msg: S) -> T;
}

impl<T> Abort<T> for Option<T> {
    fn abort<S: Display>(self, msg: S) -> T {
        if self.is_none() {
            eprintln!("ERROR!: {msg}");
            process::exit(1);
        }
        unsafe { self.unwrap_unchecked() }
    }
}
impl<T, E> Abort<T> for Result<T, E> {
    fn abort<S: Display>(self, msg: S) -> T {
        if self.is_err() {
            eprintln!("ERROR!: {msg}");
            process::exit(1);
        }
        unsafe { self.unwrap_unchecked() }
    }
}

fn main() {
    let encrypted_source_path = std::env::args()
        .next()
        .and_then(|mut p| {
            p.rfind('.').map(|pos| {
                p.truncate(pos);
                PathBuf::from(p)
            })
        })
        .abort("Couldn't get encrypted source path");
    let decrypted_dest_path = {
        let mut p = encrypted_source_path.parent().map(PathBuf::from).unwrap();
        p.push(
            encrypted_source_path
                .file_stem()
                .and_then(|s| s.to_str())
                .abort("Couldn't convert string to OsStr"),
        );
        p
    };
    if encrypted_source_path.extension().and_then(|s| s.to_str()) != Some("enc") {
        eprintln!("ERROR!: Encrypted source has an improper file extension");
        process::exit(1);
    }

    let (key, nonce) = load_key_nonce().abort("Failed to load keys");
    println!("Using key: {:?}\n    nonce: {:?}", key, nonce);

    let mut src = File::options()
        .read(true)
        .open(&encrypted_source_path)
        .abort("Couldn't open encrypted source file, does it exist?");

    let mut dst = File::options()
        .create(true)
        .read(true)
        .write(true)
        .open(&decrypted_dest_path)
        .abort("Could not open destination file");

    println!(
        "Decrypting {} -> {}",
        encrypted_source_path.display(),
        decrypted_dest_path.display()
    );

    decrypt(&mut src, &mut dst, &key, &nonce);

    println!(
        "Successfully decrypted {} -> {}",
        encrypted_source_path.display(),
        decrypted_dest_path.display()
    );
}

fn load_key_nonce() -> std::io::Result<([u8; 32], [u8; 19])> {
    fs::read("keys.dat").map(|b| {
        assert_eq!(b.len(), 51);
        let (mut key, mut nonce) = ([0u8; 32], [0u8; 19]);
        key.copy_from_slice(&b[..32]);
        nonce.copy_from_slice(&b[32..51]);
        (key, nonce)
    })
}

fn decrypt<R: std::io::Read, W: std::io::Write>(
    mut source: R,
    mut dest: W,
    key: &[u8; 32],
    nonce: &[u8; 19],
) {
    let aead = XChaCha20Poly1305::new(key.as_ref().into());
    let mut stream_decryptor = stream::DecryptorBE32::from_aead(aead, nonce.as_ref().into());

    const _BUFFER_SZ: usize = BUFFER_SZ + 16;

    let mut buffer = [0u8; _BUFFER_SZ];
    loop {
        let read = source.read(&mut buffer).unwrap();
        if read == _BUFFER_SZ {
            // Still reading
            let text = stream_decryptor.decrypt_next(buffer.as_slice()).unwrap();
            dest.write(&text).unwrap();
        } else {
            // Finish
            let text = stream_decryptor.decrypt_last(&buffer[..read]).unwrap();
            dest.write(&text).unwrap();
            break;
        }
    }
}
